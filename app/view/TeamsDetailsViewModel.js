/*
 * File: app/view/TeamsDetailsViewModel.js
 *
 * This file was generated by Sencha Architect version 3.1.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 5.0.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 5.0.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('AssignmentProject.view.TeamsDetailsViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.teamsdetails',

    requires: [
        'Ext.app.bind.Formula'
    ],

    data: {
        teamdata: null
    },

    formulas: {
        personsStore: function(get) {
            debugger;
            var persons = Ext.create('Ext.data.Store', {
                model: 'AssignmentProject.model.Person',
                data: get('teamdata').get('Persons'),
                autoSync: true
            } );

            return persons;
        },
        projectStore: function(get) {

            debugger;
            var project = Ext.create('Ext.data.Store', {
                model: 'AssignmentProject.model.Project',
                data: get('teamdata').get('Projects'),
                autoSync: true
            } );

            return project;

        }
    }

});