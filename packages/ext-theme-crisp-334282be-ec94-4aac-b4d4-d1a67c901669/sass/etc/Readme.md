# ext-theme-crisp-334282be-ec94-4aac-b4d4-d1a67c901669/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-crisp-334282be-ec94-4aac-b4d4-d1a67c901669/sass/etc"`, these files
need to be used explicitly.
